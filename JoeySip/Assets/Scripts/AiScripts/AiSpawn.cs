﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiSpawn : MonoBehaviour
{
    public GameObject aiCar;
    public Transform aiPos;
    public float repeatRate;

    private void OnCollisionEnter(Collision other)
    {
        if(other.gameObject)
        {
            InvokeRepeating("CarSpawner", 0.5f, repeatRate);
            Destroy(gameObject, 1000);
            gameObject.GetComponent<BoxCollider>().enabled = false;
        }
    }

    void CarSpawner()
    {
        Instantiate(aiCar, aiPos.position, aiPos.rotation);
    }
}
