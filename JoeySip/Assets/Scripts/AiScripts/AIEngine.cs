﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AIEngine : MonoBehaviour
{
    public Transform path;
    public float maxSteerAngle;
    public WheelCollider frontleftWheel;
    public WheelCollider frontRightWheel;
    public float maxMotorTorque;
    public float currentSpeed;
    public float maxSpeed;
    public Vector3 centerofMass;

    private List<Transform> nodes;
    private int currentNode = 0;

    private void Start()
    {
        GetComponent<Rigidbody>().centerOfMass = centerofMass;

        Transform[] pathTransforms = path.GetComponentsInChildren<Transform>();
        nodes = new List<Transform>();

        for(int i = 0; i < pathTransforms.Length; i++)
        {
            if(pathTransforms[i] != path.transform)
            {
                nodes.Add(pathTransforms[i]);
            }
        }
    }

    private void FixedUpdate()
    {
        ApplySteer();
        Drive();
        CheckWaypointDistance();
    }

    private void ApplySteer()
    {
        Vector3 relativeVector = transform.InverseTransformPoint(nodes[currentNode].position);
        float newSteer = (relativeVector.x / relativeVector.magnitude) * maxSteerAngle;
        frontleftWheel.steerAngle = newSteer;
        frontRightWheel.steerAngle = newSteer;
    }

    private void Drive()
    {
        currentSpeed = 2 * Mathf.PI * frontleftWheel.radius * frontleftWheel.rpm * 60 / 1000;
        if (currentSpeed < maxSpeed)
        {
            frontleftWheel.motorTorque = maxMotorTorque;
            frontRightWheel.motorTorque = maxMotorTorque;
        }
        else
        {
            frontleftWheel.motorTorque = 0;
            frontRightWheel.motorTorque = 0;
        }
    }

    private void CheckWaypointDistance()
    {
        if(Vector3.Distance(transform.position, nodes[currentNode].position) < 0.5f)
        {
            if(currentNode == nodes.Count - 1)
            {
                currentNode = 0;
            }
            else
            {
                currentNode++;
            }
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.CompareTag("Border"))
        {
            KillCount.killCount += 1;
            Destroy(this.gameObject);
        }
    }
}
