﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetPower : MonoBehaviour
{
    public GameObject shield;
    public GameObject playerShield;
    public SphereCollider support;
    public RawImage canvas;

    private void Start()
    {
        shield.SetActive(true);
        playerShield.SetActive(false);
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject)
        {
            shield.SetActive(false);
            playerShield.SetActive(true);
            support.GetComponent<SphereCollider>();
            support.enabled = false;
            canvas.GetComponent<RawImage>();
            canvas.enabled = true;

        }
    }
}
